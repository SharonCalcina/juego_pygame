import pygame,sys
from pygame.locals import *
from random import randint
ancho=500
alto=500
class Hunter(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.imagen=pygame.image.load("./pygame_project/images/ninja1.jpg")
        self.imagen2=pygame.image.load("./pygame_project/images/ninja11.jpg")
        self.listaImg=[self.imagen, self.imagen2]
        self.post=0
        self.hunt=self.listaImg[self.post]
        self.rect=self.hunt.get_rect()
        self.rect.centerx=ancho/2
        self.rect.centery=alto-30
        self.listaDisparo=[]
        self.velocidad=20
        self.alive=True
        self.disp=pygame.mixer.Sound('./pygame_project/sonidos/disparo.wav')
    def dibujar(self,area):
        self.hunt=self.listaImg[self.post]
        area.blit(self.hunt,self.rect)
    def behavior(self,evento):
        if evento.key==K_LEFT:
            self.post=0
        elif evento.key==K_RIGHT:
            self.post=1
    def movimiento(self,evento):
        if evento.key==K_LEFT:
            self.rect.left-=self.velocidad
            if self.rect.left<=0:
                self.rect.left=0
        elif evento.key==K_RIGHT:
            self.rect.right+=self.velocidad
            if self.rect.right>ancho:
                self.rect.right=ancho-30
    def disparo(self,x,y):
        disparar=Proyectil(x,y)
        self.listaDisparo.append(disparar)
        self.disp.play()
class Proyectil(pygame.sprite.Sprite):
    def __init__(self, posx, posy):
        pygame.sprite.Sprite.__init__(self)
        self.proyectil=pygame.image.load('./pygame_project/images/proy.png')
        self.rect=self.proyectil.get_rect()
        self.rect.top=posy
        self.rect.left=posx
        self.velocidad=4
    def trayectoria(self):
        self.rect.top=self.rect.top-self.velocidad
    def dibujar(self,area):
        area.blit(self.proyectil, self.rect)


class Fruit(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        x=randint(0,300)
        y=0
        
        self.frutilla=pygame.image.load('./pygame_project/images/frutilla.png')
        self.manzana=pygame.image.load('./pygame_project/images/manzana.png')
        self.naranja=pygame.image.load('./pygame_project/images/naranja.png')
        self.platano=pygame.image.load('./pygame_project/images/platano.png')
        self.sandia=pygame.image.load('./pygame_project/images/sandia.png')
        self.palta=pygame.image.load('./pygame_project/images/palta.png')
        self.bomba=pygame.image.load('./pygame_project/images/bomba.png')
        self.sandia2=pygame.image.load('./pygame_project/images/sandia2.png')
        self.locoto=pygame.image.load('./pygame_project/images/locoto.png')
        self.pos=randint(0,8)
        self.listaInv=[self.frutilla, self.manzana, self.naranja, self.platano, self.sandia,self.palta, self.sandia2, self.bomba,self.locoto]
        self.invader=self.listaInv[self.pos]
        self.rect=self.invader.get_rect()
        self.rect.left=x
        self.rect.top=y
        self.velocidadDisparo=1
        
    def direccion(self,val):
        pygame.time.delay(val)
        self.rect.top= self.rect.top+val
    def draw(self, area):
        area.blit(self.invader,self.rect)
    def get_position(self,invader):
        posicion=0
        if invader==self.listaInv[0]:
            posicion=0
        elif invader==self.listaInv[1]:
            posicion=1
        elif invader==self.listaInv[2]:
            posicion=2
        elif invader==self.listaInv[3]:
            posicion=3
        elif invader==self.listaInv[4]:
            posicion=4
        elif invader==self.listaInv[5]:
            posicion=5
        elif invader==self.listaInv[6]:
            posicion=6
        elif invader==self.listaInv[7]:
            posicion=7
        elif invader==self.listaInv[8]:
            posicion=8
        return posicion
    def get_invader(self,invader):
        if invader==invader==self.listaInv[7]:
            return 'bomba'
    def get_locoto(self,invader):
        if invader==invader==self.listaInv[8]:
            return 'locoto'   

def hunter_game():
    pygame.init();
    window=pygame.display.set_mode((ancho,alto))
    pygame.display.set_caption("HUNTER GAME")
    jugador= Hunter()
    disparo=Proyectil(ancho/2,alto-30)
    pygame.mixer.music.load('./pygame_project/sonidos/sonido.mp3')
    pygame.mixer.music.play(3)
#pygame.draw.line(window,color,(60,80),(160,100))
#pygame.draw.circle(window,color,(210,200),25)
#pygame.draw.rect(window,color,(20,200,100,10))
    negro=(0,0,0)
    vida=True
    fruit1=Fruit()
    fruit2=Fruit()
    fruit3=Fruit()
    fruit4=Fruit()
    fruit5=Fruit()
    fruit6=Fruit()
    fruit7=Fruit()
    fruit8=Fruit()
    fruit9=Fruit()
    fruit10=Fruit()
    fruit11=Fruit()
    fruit12=Fruit()
    listaFrutas=[fruit1,fruit2,fruit3,fruit4,fruit5,fruit6,fruit7,fruit8,fruit9,fruit10,fruit11,fruit12]
    listaf=[]
    for f in range(0,6):
        f=Fruit()
        listaf.append(f)
    fuente= pygame.font.SysFont("Arial", 40)
    text=fuente.render("GAME OVER!!!",0,(255,255,255))
    counter=0
    font= pygame.font.SysFont("Times New Roman", 30)
    #texto=font.render("Puntos"+str(counter),0,(255,255,255))
    explo=pygame.mixer.Sound('./pygame_project/sonidos/explosion.wav')
    fuentes= pygame.font.SysFont("Arial", 40)
    textos=fuente.render("OWWWW!!!",0,(255,255,255))
    moving=False
    
    
    while vida:    
        window.fill(negro)
        jugador.dibujar(window)
        disparo.dibujar(window) 
        disparo.trayectoria()
        texto=font.render("Puntos"+str(counter),0,(255,255,255))
        print(window.blit(texto,(300,300)))

        listaFrutas[0].draw(window)
        #pygame.time.delay(1)
        listaFrutas[0].direccion(3)
        listaFrutas[1].draw(window)
        listaFrutas[1].direccion(3)
        listaFrutas[2].draw(window)
        listaFrutas[2].direccion(4) 
        listaFrutas[3].draw(window)
        listaFrutas[3].direccion(5)
        listaFrutas[4].draw(window)
        listaFrutas[4].direccion(5) 
        listaFrutas[5].draw(window)
        listaFrutas[5].direccion(5) 
        listaFrutas[6].draw(window)
        listaFrutas[6].direccion(6)
        listaFrutas[7].draw(window)
        listaFrutas[7].direccion(6)
        listaFrutas[8].draw(window)
        listaFrutas[8].direccion(6) 
        listaFrutas[9].draw(window)
        listaFrutas[9].direccion(6) 
        listaFrutas[10].draw(window)
        listaFrutas[10].direccion(7)
        listaFrutas[11].draw(window)
        listaFrutas[11].direccion(7)

        listaf[0].draw(window)
        listaf[0].direccion(randint(2,5))
        listaf[1].draw(window)
        listaf[1].direccion(randint(2,7))
        listaf[2].draw(window)
        listaf[2].direccion(randint(1,6))
        listaf[3].draw(window)
        listaf[3].direccion(randint(1,5))
        listaf[4].draw(window)
        listaf[4].direccion(randint(1,4))
        listaf[5].draw(window)
        listaf[5].direccion(randint(2,7))
        if listaf[0].rect.top==alto:
            listaf[0].draw(window)
            listaf[0].direccion(randint(2,7))
        if listaf[1].rect.top==alto:
            listaf[1].draw(window)
            listaf[1].direccion(randint(2,7))
        if listaf[2].rect.top==alto:
            listaf[2].draw(window)
            listaf[2].direccion(randint(2,7))
        if listaf[3].rect.top==alto:
            listaf[3].draw(window)
            listaf[3].direccion(randint(2,7))
        if listaf[4].rect.top==alto:
            listaf[4].draw(window)
            listaf[4].direccion(randint(2,7))
        if listaf[5].rect.top==alto:
            listaf[5].draw(window)
            listaf[5].direccion(randint(2,7))
        
        
     
        if listaFrutas[0].rect.top==alto:
            listaFrutas[0]=Fruit()
            listaFrutas[1]=Fruit()
        if listaFrutas[2].rect.top==alto:
            listaFrutas[2]=Fruit()
            
        if listaFrutas[3].rect.top==alto :
            listaFrutas[3]=Fruit()
            listaFrutas[4]=Fruit()
            listaFrutas[5]=Fruit()
             
        if listaFrutas[6].rect.top==alto:
            listaFrutas[6]=Fruit()
            listaFrutas[7]=Fruit() 
            listaFrutas[8]=Fruit()
            listaFrutas[9]=Fruit()
        if listaFrutas[10].rect.top==alto:
            listaFrutas[10]=Fruit()
            listaFrutas[11]=Fruit()
       


        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit
                sys.exit
                pygame.quit()
            elif vida ==True:
                if event.type==pygame.KEYDOWN:
                    jugador.movimiento(event)
                    jugador.behavior(event) 
                    if event.key==K_SPACE:
                        x,y=jugador.rect.center
                        jugador.disparo(x,y)
           
        if len(jugador.listaDisparo)>0:
            for k in jugador.listaDisparo:
                k.dibujar(window)
                k.trayectoria()
                if k.rect.top<-10:
                    jugador.listaDisparo.remove(k)
                elif k.rect.top>100:
                    p=0                    
                    for enemy in listaFrutas:
                        if k.rect.colliderect(enemy.rect):
                            #jugador.listaDisparo.remove(k)
                            if enemy.get_invader(enemy.invader)=='bomba':
                                listaFrutas.remove(enemy)
                                nuevo=Fruit()
                               # listaFrutas[p]=nuevo
                                listaFrutas.append(nuevo)
                                counter=counter-5
                                explo.play()
                        p+=1
                elif k.rect.top>100:
                    q=0
                    for enemies in listaf:
                        if k.rect.colliderect(enemies.rect):
                            #jugador.listaDisparo.remove(k)
                            if enemies.get_invader(enemies.invader)=='bomba':
                                listaf.remove(enemies)
                                nuevo=Fruit()
                                #listaf[q]=nuevo
                                listaf.append(nuevo)
                                counter=counter+5
                                explo.play()
                        q=+1
        m=0
        for enemy in listaFrutas:
            if enemy.rect.bottom>460:
                if jugador.rect.colliderect(enemy.rect):
                    if enemy.get_invader(enemy.invader)!='bomba' and enemy.get_invader(enemy.invader)!='locoto':
                        pos=enemy
                        listaFrutas.remove(enemy)
                        nuevo=Fruit()
                        #listaFrutas[m]=nuevo
                        listaFrutas.append(nuevo)
                        counter+=1
                        print('---------punto ----------')
                    elif enemy.get_invader(enemy.invader)=='bomba':
                        explo.play()
                        listaFrutas.remove(enemy)
                        nuevo=Fruit()
                        print(window.blit(textos,(300,350)))
                        counter=counter-5
                        #listaFrutas[m]=nuevo
                        listaf.append(nuevo)
                    elif enemy.get_locoto(enemy.invader)=='locoto':
                        counter=counter-1
                        print(window.blit(textos,(300,350)))
            m+=1
        
        n=0
        for enemyes in listaf:
            if enemyes.rect.bottom>460:
                if jugador.rect.colliderect(enemyes.rect):
                    if enemyes.get_invader(enemyes.invader)!='bomba' and enemyes.get_locoto(enemyes.invader)!='locoto':
                        pos=enemyes
                        listaf.remove(enemyes)
                        nuevo=Fruit()
                        #listaf[]=nuevo
                        listaf.append(nuevo)
                        counter+=1
                        print('-------done')
                        #listaf.append(nuevo)
                        
                    elif enemyes.get_invader(enemyes.invader)=='bomba':
                        explo.play()
                        pos=enemyes
                        listaf.remove(enemyes)
                        nuevo=Fruit()
                        #listaf[n]=nuevo
                        listaf.append(nuevo)
                        print(window.blit(textos,(300,350)))
                        counter=counter-5
                    elif enemyes.get_locoto(enemyes.invader)=='locoto':
                        counter=counter-1
                        print(window.blit(textos,(300,350)))
            n+=1

        pygame.display.update()
hunter_game()

